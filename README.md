# AWS Route53 Updater Service

Service to update main domain A record IP address value in AWS Route53 when external IP address changes.

## Getting Started

### Install .NET Core Runtime
Download and install the .NET Core 3.1 Runtime (I use the ASP.NET Core Runtime)

[https://dotnet.microsoft.com/download/dotnet-core/3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)

### Update aws.json
This file needs populating with your AWS credentials.
Optionally change Check Delay if needed. This is how often it will check your external IP address.

### Publish the service

Publish as Release (recommended) either using visual studio or alternatively using cli with: `dotnet publish -c Release -o c:\bin\example`

### Register the service
Open up cmd as administrator and run: `sc create "Service Name" binPath=c:\bin\example\Route53UpdateService.exe`


### Run the service
Start using services.msc or `sc start "Service Name"`