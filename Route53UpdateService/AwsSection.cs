﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Route53UpdateService
{
    class AwsSection
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public int CheckDelay { get; set; }
    }
}
