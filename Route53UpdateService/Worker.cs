using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Route53;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Route53UpdateService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IAmazonRoute53 _route53;
        private readonly IConfiguration _configuration;

        private string _ipAddress;
        private int _checkDelay;



        public Worker(ILogger<Worker> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            AwsSection awsSection = _configuration.GetSection("AWS").Get<AwsSection>();
            _checkDelay = awsSection.CheckDelay;
            _route53 = new AmazonRoute53Client(new Amazon.Runtime.BasicAWSCredentials(awsSection.AccessKey, awsSection.SecretKey), Amazon.RegionEndpoint.EUWest1);

        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.CompletedTask;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Service Starting");

            while(!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    string externalip = new System.Net.WebClient().DownloadString("http://ipinfo.io/ip").Trim();

                    if (_ipAddress is null || _ipAddress != externalip)
                    {
                        Amazon.Route53.Model.ListHostedZonesResponse hostedZones = await _route53.ListHostedZonesAsync();

                        foreach (Amazon.Route53.Model.HostedZone hostedZone in hostedZones.HostedZones)
                        {
                            Amazon.Route53.Model.ListResourceRecordSetsResponse recordSets = await _route53.ListResourceRecordSetsAsync(new Amazon.Route53.Model.ListResourceRecordSetsRequest
                            {
                                HostedZoneId = hostedZone.Id

                            });

                            Amazon.Route53.Model.ResourceRecordSet mainRecordSet = recordSets.ResourceRecordSets.SingleOrDefault(x => x.Type == RRType.A && x.Name == hostedZone.Name);

                            if (mainRecordSet != null)
                            {
                                mainRecordSet.ResourceRecords[0].Value = externalip;
                                Amazon.Route53.Model.ChangeResourceRecordSetsResponse changeResponse = await _route53.ChangeResourceRecordSetsAsync(new Amazon.Route53.Model.ChangeResourceRecordSetsRequest
                                {
                                    HostedZoneId = hostedZone.Id,
                                    ChangeBatch = new Amazon.Route53.Model.ChangeBatch(new List<Amazon.Route53.Model.Change>()
                                {
                                    new Amazon.Route53.Model.Change(ChangeAction.UPSERT, mainRecordSet)
                                })
                                });

                                if (changeResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    _logger.LogInformation("Hosted Zone IP Updated: [" + hostedZone.Name + "] [" + externalip + "]");
                                }
                            }
                        }

                        _ipAddress = externalip;
                    }
                }
                catch { }
                Thread.Sleep(_checkDelay);
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping Service");
            await base.StopAsync(cancellationToken);
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
