using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Route53;
using Amazon.Extensions;
using Amazon.Extensions.NETCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Route53UpdateService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("aws.json", optional: false, reloadOnChange: true);                    

                })
                /*
                 * When the application is hosted in a Windows Service, 
                 * the extension method IHostBuilder.UseWindowsService() will set the ContentRoot, 
                 * configure logging, set the host lifetime to WindowsServiceLifetime, and so on.
                 */
                .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddAWSService<IAmazonRoute53>();
                    services.AddHostedService<Worker>();
                });
    }
}
